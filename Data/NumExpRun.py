#!/bin/python
import numpy as np
import scipy as sp
import os, shutil, sys, re
from matplotlib import pyplot as plt
from scipy.constants import codata
from mpl_toolkits.mplot3d import Axes3D
''' The script runs a set of numerical experiments
in an automated fashion '''


#default parameters
refDir="refData";
workDir=".";
Nref = range(2,7);
samples = 50
omega = np.concatenate([np.array([0]),3*np.logspace(1,8,samples)]);
IOR = [1, 2.42];
Domain = ["ID1"]#, "ID2"]
shift = [ np.array([-0.47619,0.66667]), np.array([-1.0/3,-0.047619]), np.array([-0.99048,0.438095]), np.array([-1.00952,2.70476]) ];
#shift = [ np.array([-0.99048,0.438095]), np.array([-1.00952,2.70476]) ];
tol = 1e-11;
Neig = 10;
executable='fem_eigenvaluedecomposition_P3_Full_MW04Simplified'


if __name__=='__main__':
    #1. parse command line parameters
    while len(sys.argv)>1:
        option = sys.argv[1]
        del sys.argv[1];
        if option=='-refdir':
            refDir = str(sys.argv[1]);  del sys.argv[1];
        elif option=='-workdir':
            workDir = str(sys.argv[1]);  del sys.argv[1];
        elif option=='-nref':
            rawData = str(sys.argv[1]); del sys.argv[1];
            data = re.split(',', rawData);
            Nref = [int(x) for x in data];
        elif option=='-ior':
            rawData = str(sys.argv[1]); del sys.argv[1];
            data = re.split(',', rawData);
            IOR = [float(x) for x in data];
        elif option == '-domain':
            rawData = str(sys.argv[1]); del sys.argv[1];
            Domain = [str(x) for x in rawData.split(',')];
        elif option == '-runfile':
            rawData = str(sys.argv[1]); del sys.argv[1];
            executable = rawData;
        elif option == '-samples':
            rawData = int(sys.argv[1]); del sys.argv[1];
            samples = rawData;
            
    omega = np.concatenate([np.array([0]),3*np.logspace(1,8,samples)]);
    #Check whether the reference data for the given domains exists
    if refDir == 'refData':
        refDir = os.path.abspath(".")+"/"+refDir;     
    if os.path.isdir(refDir) == False:
        print "Error! The reference directory", refDir, " does not exist";
        sys.exit(1);
        #absolute path to wroking directory
    if workDir == '.':
        workDir = os.path.abspath(workDir);
    #check whether the geometry files are there
    files = os.listdir(refDir);
    for D in Domain:
        Datalist = [D+"_1_Coord.dat", D+"_2_Coord.dat", D+"_1_Elements.dat", D+"_2_Elements.dat",
        D+"_1_Boundary.dat",D+"_2_Boundary.dat"];
        for data in Datalist:
            filepath = refDir +'/'+data;
            if os.path.exists(filepath) == False:
                print "Reference data ", data, "does not exist!"
                sys.exit(1);
    #write the file of angular frequencies
    os.chdir(refDir);
    omegaFile = file("AngularFrequencies.dat", 'w');
    for i in omega:
        omegaFile.write("%.6g\n"%(i));
    omegaFile.close();
    os.chdir(workDir);
    #2. Create a list of all cases to be treated.
    ExperimentList = [];
    for D in Domain:
        for n in IOR:
            #set up experimental data
            experiment = {};
            experiment['domain']=D;
            experiment['ior']=n;
            experiment['omega']=refDir+"/AngularFrequencies.dat";
            experiment['nref']=Nref;
            
            ExperimentList.append(experiment);
       
    #3. Store the list of all cases in a file in the
    #    current working directory
    experimentFile = np.save("ExperimentCases", ExperimentList);
    #4. Run the numerical experiments
    for i in xrange(len(ExperimentList)):
        #create experiment directory
        expDir = workDir + "/Exp_"+str(i);
        if os.path.isdir(expDir):
            print "ERROR! The experiment directory ", expDir, " exists"
            sys.exit(1);
        os.mkdir(expDir);
        #change into directory
        os.chdir(expDir);
        for subID in [1,2]:
            ElementFile = ExperimentList[i]['domain']+"_%d_Elements.dat"%(subID);
            CoordFile = ExperimentList[i]['domain']+"_%d_Coord.dat"%(subID);
            BoundaryFile = ExperimentList[i]['domain']+"_%d_Boundary.dat"%(subID);
            OmegaFile = ExperimentList[i]['omega']
            #link the mesh files from the refernce directory
            #link the coordinate file
            src = refDir+"/"+CoordFile;
            dst = expDir+"/"+CoordFile;
            os.symlink(src,dst);
            #link the boundary file
            src = refDir+"/"+BoundaryFile;
            dst = expDir+"/"+BoundaryFile;
            os.symlink(src,dst);
            #link the element file
            src = refDir+"/"+ElementFile;
            dst = expDir+"/"+ElementFile;
            os.symlink(src,dst);
            #link the angular frequencies file
            dst = expDir+"/AngularFrequencies.dat";
	    if os.path.isfile(dst) == False:
	            os.symlink(OmegaFile,dst);
            #create parameter file
            params = '''CF=%s
EF=%s
BFD=%s
n=%f
omega=%s
tol=%g
FP=%s
Neig=%d
xShft=%f
yShft=%f\n'''%(CoordFile, ElementFile, BoundaryFile, ExperimentList[i]['ior'],\
            "AngularFrequencies.dat", tol, D+"_"+str(subID), Neig, shift[subID-1][0],\
            shift[subID-1][1]);
            #write parameter file
            paramFile = file("parameters.txt", 'w')
            paramFile.write(params)
            paramFile.close()           
            #run experiment
            for N in ExperimentList[i]['nref']:
                cmd = refDir+"/"+executable + " --N "+str(N);
                failure = os.system(cmd);
                if failure:
                    print "Simulation with N =",N, " failed. Exiting..."
                    sys.exit(1);
                #rename parameter files
             #   os.rename("parameters.txt", "parameters_N%d.txt"%(N))
                #rename output
                os.rename("output.dat", "output_N%d_%s_%d.dat"%(N,ExperimentList[i]['domain'], subID))
        #change back to working directory
        os.chdir(workDir);
        
#    ########################################################################
#    #EVALUATION OF THE EXPERIMENTS
#    ########################################################################
#    #1. Load the list of experiments
#    f = open("ExperimentCases.npy")
#    ExperimentList = np.load(f);
#    #2. Go into each data directory
#    for i in xrange(len(ExperimentList)):
#        #create experiment directory
#        expDir = workDir + "/Exp_"+str(i);
#        #check if directory exists
#        if os.path.isdir(expDir) == False:
#            print "ERROR! The experiment directory ", expDir, " exists"
#            sys.exit(1);
#        #change into directory
#        os.chdir(expDir);
#        #dict for storing accumulated differences
#        results = {}
#        #iterate over the refinement levels
#        for N in ExperimentList[i]['nref']:        
#                #2.1 Load the data for both subdomains
#                datafile1 = open("output_N%d_%s_1.dat"%(N,ExperimentList[i]['domain']))
#                datafile2 = open("output_N%d_%s_2.dat"%(N,ExperimentList[i]['domain']))
#                omega = []
#                EigVal1 = []
#                EigVal2 = []
#                lineIdx = 0;
#                for line in datafile1:
#                    data = line.split();
#                    #2.1.1 Load the first row into omega
#                    if lineIdx == 0:
#                        for el in data:
#                            omega.append(float(el));
#                    #2.1.2 Load the rest of the rows into the eigenvalue array
#                    else:
#                        EigValRow = []
#                        for el in data:
#                            EigValRow.append(float(el.split('+')[0]));
#                        EigVal1.append(EigValRow);
#                    lineIdx +=1;
#                #same procedure for the second domain
#                lineIdx = 0;
#                for line in datafile2:
#                    data = line.split();
#                    #skip the omegas
#                    #2.1.2 Load the rest of the rows into the eigenvalue array
#                    if lineIdx == 0:
#                        lineIdx+=1;
#                        continue;
#                    else:
#                        EigValRow = []
#                        for el in data:
#                            EigValRow.append(float(el.split('+')[0]));
#                        EigVal2.append(EigValRow);
#                    lineIdx+=1;
#                #2.2 For all eigenvalues (using stride 2 due to multiplicity) do
#                #2.2.1 Plot |lambda_1 -lambda_2|(omega) vs R0*omega/c
#                EVArr1 = np.array(EigVal1);
#                EVArr2 = np.array(EigVal2);
#                EVDiff = abs(EVArr1[0:-1:2,:]-EVArr2[0:-1:2,:])
#                omega = np.array(omega);
#                c = sp.constants.value('speed of light in vacuum');
#                xVal = abs(np.sqrt(3.**2+1**2)*omega/c);
#
#                Neig = np.shape(EVDiff)[0];
#                #save data
#                results["%d"%(N)] = EVDiff;
#                fig, host = plt.subplots(figsize=(11,8))
#                #skip the first line of data - stationary eigenvalues
#                for k in range(Neig/2):
#                    host.loglog(xVal[1:], EVDiff[Neig-1-k, 1:], label="$\lambda_%d$"%(k));
#                host.loglog(xVal[1:], EVDiff[1,1:], 'k+--',label="$\lambda_9$")
#                plt.xlabel("$\\frac{\omega R}{c}$")
#                plt.ylabel("$|(\lambda^{(1)})^2 - (\lambda^{(2)})^2|$")
#                host.grid();
#                plt.title("Deviation of the eigenvalues. meshsize=%g, ior=%g"%(2./2**N, ExperimentList[i]['ior']))
#                plt.legend(loc='best')
#                #plt.show();
#                plt.savefig("EVDiff_vs_RotFreq_RefLevel_%d_ior_%g.png"%(N,ExperimentList[i]['ior']),format='png',papertype='a4', orientation='landscape');
#                plt.close();
#                
#                #2.2.3 Plot |lambda_1 - lambda_2|(omega)/|lambda_1 - lambda_2|(0)
#                fig, host = plt.subplots(figsize=(11,8))
#                for k in range(Neig/2 ):
#                    host.loglog(xVal[1:], EVDiff[Neig-1-k, 1:]/EVDiff[Neig-1-k,0], label="$\lambda_%d$"%(k));
#                host.loglog(xVal[1:], EVDiff[1,1:]/EVDiff[1,0], 'k+--',label="$\lambda_9$")
#                plt.xlabel("$\\frac{\omega R}{c}$")
#                plt.ylabel("$\\frac{|(\lambda^{(1)})^2 - (\lambda^{(2)})^2|(\omega)}{|(\lambda^{(1)})^2 - (\lambda^{(2)})^2|(0)}$")
#                host.grid();
#                plt.title("Deviation of the eigenvalues. meshsize=%g, ior=%g"%(2./2**N, ExperimentList[i]['ior']))
#                plt.legend(loc='best')                
#                #plt.show();
#                plt.savefig("EVDiff_vs_RotFreq_Normalized_RefLevel_%d_ior_%g.png"%(N,ExperimentList[i]['ior']),format='png',papertype='a4', orientation='landscape');
#                plt.close();
#                
#                #plot the evolution of the 9th eigenvalue
#                fig, host = plt.subplots(figsize=(11,8))
#                host.semilogx(xVal[1:], -EVArr1[2,1:], 'r:^', xVal[1:], -EVArr2[2,1:], 'b:^')
#                plt.xlabel("$\\frac{\omega R}{c}$")
#                plt.ylabel("$|\lambda^2|(\omega)$")
#                host.grid();
#                plt.title("Behaviour of the analytic eigenvalues. meshsize=%g, ior=%g"%(2./2**N, ExperimentList[i]['ior']))
#                plt.legend(("Domain 1", "Domain 2"), loc='best')                
#                #plt.show();
#                plt.savefig("AnalyticEigenvalueEvolution_RefLevel_%d_ior_%g.png"%(N,ExperimentList[i]['ior']),format='png',papertype='a4', orientation='landscape');
#                plt.close();
#                #same but scaled
#                fig, host = plt.subplots(figsize=(11,8))
#                host.loglog(xVal[1:], EVArr1[2,1:]/EVArr1[2,0], 'r:^', xVal[1:], EVArr2[2,1:]/EVArr2[2,0], 'b:^')
#                plt.xlabel("$\\frac{\omega R}{c}$")
#                plt.ylabel("$|\lambda^2|(\omega)$")
#                host.grid();
#                plt.title("Behaviour of the analytic eigenvalues (normalized). meshsize=%g, ior=%g"%(2./2**N, ExperimentList[i]['ior']))
#                plt.legend(("Domain 1", "Domain 2"), loc='best')                
#                #plt.show();
#                plt.savefig("AnalyticEigenvalueEvolution_Normalized_RefLevel_%d_ior_%g.png"%(N,ExperimentList[i]['ior']),format='png',papertype='a4', orientation='landscape');
#                plt.close();                
#        #append the angular frequencies
#        results['omega'] = omega;
#        #store results
#        savefile = file("Results_Exp_%d.npz"%(i), 'w');
#        np.savez(savefile, results);
#        savefile.close();
#        #plot the behaviour of the ground state and the 9th eigenstate as functions of N
#        GSEVDiff = np.zeros((len(ExperimentList[i]['nref']), len(results['omega'])-1) );
#        NinthEVDiff = np.zeros((len(ExperimentList[i]['nref']), len(results['omega'])-1) );
#        
#        for k in range(len(ExperimentList[i]['nref'])):
#            GSEVDiff[k] = results["%d"%(ExperimentList[i]['nref'][k])][Neig-1,1:];
#            NinthEVDiff[k] = results["%d"%(ExperimentList[i]['nref'][k])][1,1:];
#            
#        #prepare for 3D plot
#        meshSize = 2.0/np.logspace(min(ExperimentList[i]['nref']),max(ExperimentList[i]['nref']),base=2,num=len(ExperimentList[i]['nref']) );
#        xVal = abs(results['omega'][1:]*np.sqrt(3.**2+1**2)/c);
#        X, Y = np.meshgrid(np.log10(xVal), np.log2(meshSize) );
#        fig = plt.figure(figsize=(11,8));
#        ax = fig.gca(projection='3d');
#        ax.plot_wireframe(X,Y, np.log10(GSEVDiff), rstride=1, cstride=1, color='red',label="$\lambda_0$");
#        ax.plot_wireframe(X,Y, np.log10(NinthEVDiff), rstride=1, cstride=1, color='blue',label="$\lambda_9$");
#        xticks = ax.get_xticks()
#        xticklabels = ["$10^{%d}$"%(x) for x in xticks]
#        yticks = [2.0/(2**x) for x in ExperimentList[i]['nref'] ]
#        yticklabels = ["$\\frac{2}{2^{%d}}$"%(x) for x in ExperimentList[i]['nref'] ]
#        zticks = ax.get_zticks()
#        zticklabels = ["$10^{%d}$"%(x) for x in zticks];
#        #ax.set_xticks(np.log10(xticks))
#        ax.set_yticks(np.log2(yticks));
#        #ax.set_zticks(np.log10(zticks));
#        ax.set_xticklabels(xticklabels);
#        ax.set_yticklabels(yticklabels);
#        ax.set_zticklabels(zticklabels);
#        ax.set_xlabel("$\\frac{R_0\omega}{c}$")
#        ax.set_ylabel("mesh size")
#        ax.set_zlabel("$|(\lambda^{(1)})^2 - (\lambda^{(2)})^2|$")
#        ax.legend()
#        plt.show()
#            
#        #go back to the working directory
#        os.chdir(workDir);
