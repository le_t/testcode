from matplotlib import pyplot as plt
import numpy as np
import matplotlib.tri as tri
from scipy.constants import codata
import scipy as sp

c = sp.constants.value('speed of light in vacuum');

vertFile = open("Plot_Vertices_ID2_1.dat", 'r')

vertices = []
for line in vertFile:
    data = line.split();
    vertex = [float(data[0]), float(data[1]) ];
    vertices.append(vertex);
    
    
triFile = open("Plot_Elements_ID2_1.dat", 'r')
triangles = []
for line in triFile:
    data = line.split();
    triangle = [int(data[0]), int(data[1]), int(data[2])]
    triangles.append(triangle)

vertexArray = np.asarray(vertices)
triangleArray = np.asarray(triangles)
triangulationOwn = tri.Triangulation(vertexArray[:,0], vertexArray[:,1], triangles=triangleArray)

#load the angular frequencies
omegaFile = open("AngularFrequencies.dat", 'r')
omega = []
for line in omegaFile:
    data = float(line)
    omega.append(data)
omega = np.array(omega)

#iterate over the array in steps of 5
for k in range(len(omega)):
    if (5*k > len(omega)):
        break;
    omegaValue = omega[k*5]
    #load the eigenvalues
    EVFile = open("Eigenvalues_ID2_1_omega_%.5f.dat"%(omegaValue), 'r')
    eigval = []
    for line in EVFile:
        data = line.split();
        ev = []
        for x in data:
            x = x.replace('(', ' ')
            x = x.replace(",-", '-')
            x = x.replace(',', '+')
            x = x.replace(')', 'j')
            ev.append(complex(x));
        eigval.append(ev)
    evArray = np.array(eigval)
    
    for k in xrange(np.shape(evArray)[1]):
        #plt.subplot(121)
        plt.figure()
        plt.gca().set_xticklabels([])
        plt.gca().set_yticklabels([])
        plt.gca().set_aspect('equal')
        plt.tripcolor(triangulationOwn, np.real(evArray[:,k]), shading='gouraud', cmap=plt.cm.viridis)
        #plt.colorbar()
        plt.title("Eigenmode %d+, $\\frac{\omega}{c}=%.3e$"%(k,omegaValue/c))
        #plt.show()
        plt.savefig("Eigenmode_%d_sum_omega_%.0f.jpg"%(k,omegaValue), format='jpg', papertype='a4', dpi=300, orientation='landscape')
        #plt.savefig("Eigenmode_%d.eps"%(k), format='eps', papertype='a4', dpi=300, orientation='landscape')
        #plt.subplot(122)
        #plt.gca().set_xticklabels([])
        #plt.gca().set_yticklabels([])
        #plt.gca().set_aspect('equal')
        #plt.tripcolor(triangulationOwn, np.real(evArray[:,2*k] - evArray[:,2*k+1]), shading='gouraud', cmap=plt.cm.viridis)
        #plt.colorbar()
        #plt.title("Eigenmode %d-, $\\frac{\omega}{c}=%.3e$"%(k,omegaValue/c))
        ##plt.show()
        #plt.savefig("Eigenmode_%d_diff_omega_%.0f.png"%(k,omegaValue), format='png', papertype='a4', dpi=300, orientation='landscape')
        ##plt.savefig("Eigenmode_%d.eps"%(k), format='eps', papertype='a4', dpi=1000, orientation='landscape')
        plt.close()