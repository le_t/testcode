#!/usr/bin/python
''' The script is used to automatically generate the plots of eigenmodes for the
two pairs of isospectral domains (ID1, ID2) where the computations have been
performed both with the linearized PDE and the full PDE for vacuum and diamond.

The routine for plotting the eigenmodes for one domain and one type of equation 
is demonstrated in the IPython notebook "EigenmodePlots.ipynb".
'''
from matplotlib import pyplot as plt
import numpy as np
import matplotlib.tri as tri
from scipy.constants import codata
import scipy as sp
import matplotlib.colors
from mpl_toolkits.axes_grid1 import AxesGrid
import os, shutil, sys
from PlotFunctions import *

c = sp.constants.value('speed of light in vacuum');

#define the dictionary containing the indices of the eigenvectors to plot
#for each domain and equation type
#the indexing is [GS_stat_vac, GS_low_vac, GS_med_vac, GS_high_vac, ..., NS_high_dia]
IndexDict = { ("ID1_1","full"):[[2,0,1,1], [17,16,17,16],  [0,0,0,0], [-17,16,16,-17] ]#,
#("ID1_1", "lin"):[ [-1,1,1,0 ], [16,16,16,16 ], [0,1,1,0 ], [16,16,16,16]  ],
#("ID2_1","full"):[ [0,0,0,2], [], [0,0,1,0], [] ],
#("ID2_1", "lin"):[ [0,0,0,0], [], [0,0,0,0], [] ]
};
#similarly define a dictionary of the folders where the data is stored
FolderDict = { ("ID1_1","full"):"/home/shared/FEM_EV_NumExp/Data/CorrectedShift/P1_Full_EigVecExperiment", 
("ID1_1", "lin"):"/home/shared/FEM_EV_NumExp/Data/CorrectedShift/P1_EigVecExperiment",
("ID2_1","full"):"/home/shared/FEM_EV_NumExp/Data/ID2/P1_Full_EigVecExperiment",
("ID2_1", "lin"):"/home/shared/FEM_EV_NumExp/Data/ID2/P1_EigVecExperiment",
};
#finally define a dictionary with the centres of mass (precomputed for the chosen 
#triangulations)
CoM = {"ID1_1":np.array([-0.47619,0.66667]) ,
"ID1_2":np.array([-1.0/3,-0.047619]),
"ID2_1":np.array([-0.99048,0.438095]),
"ID2_2":np.array([-1.00952,2.70476])
};

if __name__=='__main__':
    #store current folder
    origin = os.getcwd();
    
    for key in IndexDict.keys():
        print key
        print FolderDict[key]
        #fetch directory and step into it
        Directory = FolderDict[key];
        if os.path.isdir(Directory) == False:
            print "ERROR! The experiment directory ", Directory, " does not exist"
            sys.exit(1);
        os.chdir(Directory);
        #fetch the indices
        Indices = IndexDict[key];
        #go into the Vacuum-subdirectory
        os.chdir("./Exp_0")
        (GSIdx_stat, GSIdx_low, GSIdx_med, GSIdx_high) = Indices[0];
        #signs of the eigenmodes
        signsGS = np.signbit( np.array(Indices[0]) );
        
        #fetch triangulation
        domain = key[0];
        
        vertexFilename = "Plot_Vertices_"+domain+".dat"
        elementFilename = "Plot_Elements_"+domain+".dat"
        ModeFile_low = "Eigenvalues_"+domain+"_omega_661.53900.dat"
        ModeFile_med = "Eigenvalues_"+domain+"_omega_58891200.00000.dat"
        ModeFile_high = "Eigenvalues_"+domain+"_omega_132919000.00000.dat"
        
        triangulationOwn = readTriangulation(vertexFilename, elementFilename);
        #read the eigenmode data and extract the appropriate eigenvectors
        ModeFile_stat = "Eigenvalues_"+domain+"_omega_0.00000.dat";
        EVstat = readEigenmodes(ModeFile_stat)
        if signsGS[0] == True:
            GSstat = -1*EVstat[:,GSIdx_stat];
        else:
            GSstat = EVstat[:,GSIdx_stat];
        
        
        EVlow = readEigenmodes(ModeFile_low);
        EVmed = readEigenmodes(ModeFile_med);
        EVhigh = readEigenmodes(ModeFile_high);
        
        GS = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );
        if signsGS[1] == True:
            GS[:,0] = -1*EVlow[:,GSIdx_low];
        else:
            GS[:,0] = EVlow[:,GSIdx_low];
            
        if signsGS[2] == True:
            GS[:,1] = -1*EVmed[:,GSIdx_med];
        else:
            GS[:,1] = EVmed[:,GSIdx_med];
            
        if signsGS[3] == True:
            GS[:,2] = -1*EVhigh[:,GSIdx_high];
        else:
            GS[:,2] = EVhigh[:,GSIdx_high];
        
        #compute the differences
        GSdiff = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );

        GSdiff[:,0] = GS[:,0] - GSstat
        GSdiff[:,1] = GS[:,1] - GSstat
        GSdiff[:,2] = GS[:,2] - GSstat
        
        #colorbar normalization
        vmaxGS = max(abs(GS[:,0:2].flatten()))
        vminGS = np.real( min(GS[:,0:2].flatten()) )
        normGS = matplotlib.colors.Normalize(vmax=vmaxGS, vmin=vminGS)
        
        vmaxGSdiff = max(abs(GSdiff[:,0:2].flatten()))
        vminGSdiff = np.real( min(GSdiff[:,0:2].flatten()) )
        normGSdiff = matplotlib.colors.Normalize(vmax=vmaxGSdiff, vmin=vminGSdiff)
        
        #plot
        PlotEigenmodes(GS, GSdiff, triangulationOwn, normGS, normGSdiff, show=True, save=False, drawOrigin=True)
       
        #skip the evalueation of the eigenmodes for which no indices were given
        if Indices[1] == []:
            continue;
        
        (NSIdx_stat, NSIdx_low, NSIdx_med, NSIdx_high) = Indices[1];
        signsNS = np.signbit( np.array(Indices[1]) );
        
        if signsNS[0] == True:
            NSstat = -1*EVstat[:,NSIdx_stat];
        else:
            NSstat = EVstat[:,NSIdx_stat];
            
        NS = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );
        
        if signsNS[1] == True:
            NS[:,0] = -1*EVlow[:,NSIdx_low];
        else:
            NS[:,0] = EVlow[:,NSIdx_low];
            
        if signsNS[2] == True:
            NS[:,1] = -1*EVmed[:,NSIdx_med];
        else:
            NS[:,1] = EVmed[:,NSIdx_med];
            
        if signsNS[3] == True:
            NS[:,2] = -1*EVhigh[:,NSIdx_high];
        else:
            NS[:,2] = EVhigh[:,NSIdx_high];
        
        #differences
        NSdiff = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );
        NSdiff[:,0] = NS[:,0] - NSstat
        NSdiff[:,1] = NS[:,1] - NSstat
        NSdiff[:,2] = NS[:,2] - NSstat
        
        #colorbar normalization
        vmaxNS = max(abs(NS[:,0:2].flatten()))
        vminNS = np.real( min(NS[:,0:2].flatten()) )
        normNS = matplotlib.colors.Normalize(vmax=vmaxNS, vmin=vminNS)
        
        vmaxNSdiff = max(abs(NSdiff[:,0:2].flatten()))
        vminNSdiff = np.real( min(NSdiff[:,0:2].flatten()) )
        normNSdiff = matplotlib.colors.Normalize(vmax=vmaxNSdiff, vmin=vminNSdiff)
        
        PlotEigenmodes(NS, NSdiff, triangulationOwn, normNS, normNSdiff, show=True, save=False, drawOrigin=True)
        
        ########################################################################
        #do the same for diamond
        os.chdir("../Exp_1");
        (GSIdx_stat, GSIdx_low, GSIdx_med, GSIdx_high) = Indices[2];
        signsGS = np.signbit( np.array(Indices[2]) );
        
        #fetch triangulation
        domain = key[0];
        
        vertexFilename = "Plot_Vertices_"+domain+".dat"
        elementFilename = "Plot_Elements_"+domain+".dat"
        ModeFile_low = "Eigenvalues_"+domain+"_omega_661.53900.dat"
        ModeFile_med = "Eigenvalues_"+domain+"_omega_58891200.00000.dat"
        ModeFile_high = "Eigenvalues_"+domain+"_omega_132919000.00000.dat"
        
        triangulationOwn = readTriangulation(vertexFilename, elementFilename);
        #read the eigenmode data and extract the appropriate eigenvectors
        ModeFile_stat = "Eigenvalues_"+domain+"_omega_0.00000.dat";
        EVstat = readEigenmodes(ModeFile_stat);
        
        if signsGS[0] == True:
            GSstat = -1*EVstat[:,GSIdx_stat];
        else:
            GSstat = EVstat[:,GSIdx_stat];
        
        
        EVlow = readEigenmodes(ModeFile_low);
        EVmed = readEigenmodes(ModeFile_med);
        EVhigh = readEigenmodes(ModeFile_high);
        
        GS = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );
        if signsGS[1] == True:
            GS[:,0] = -1*EVlow[:,GSIdx_low];
        else:
            GS[:,0] = EVlow[:,GSIdx_low];
            
        if signsGS[2] == True:
            GS[:,1] = -1*EVmed[:,GSIdx_med];
        else:
            GS[:,1] = EVmed[:,GSIdx_med];
            
        if signsGS[3] == True:
            GS[:,2] = -1*EVhigh[:,GSIdx_high];
        else:
            GS[:,2] = EVhigh[:,GSIdx_high];
        
        #compute the differences
        GSdiff = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );
        GSdiff[:,0] = GS[:,0] - GSstat
        GSdiff[:,1] = GS[:,1] - GSstat
        GSdiff[:,2] = GS[:,2] - GSstat
        
        #colorbar normalization
        vmaxGS = max(abs(GS[:,0:2].flatten()))
        vminGS = np.real( min(GS[:,0:2].flatten()) )
        normGS = matplotlib.colors.Normalize(vmax=vmaxGS, vmin=vminGS)
        
        vmaxGSdiff = max(abs(GSdiff[:,0:2].flatten()))
        vminGSdiff = np.real( min(GSdiff[:,0:2].flatten()) )
        normGSdiff = matplotlib.colors.Normalize(vmax=vmaxGSdiff, vmin=vminGSdiff)
        
        #plot
        PlotEigenmodes(GS, GSdiff, triangulationOwn, normGS, normGSdiff, show=True, save=False, drawOrigin=True)
       
        #skip the evalueation of the eigenmodes for which no indices were given
        if Indices[3] == []:
            continue;
        
        (NSIdx_stat, NSIdx_low, NSIdx_med, NSIdx_high) = Indices[3];
        signsNS = np.signbit( np.array(Indices[3]) );
        
        if signsNS[0] == True:
            NSstat = -1*EVstat[:,NSIdx_stat];
        else:
            NSstat = EVstat[:,NSIdx_stat];
            
        NS = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );
        
        if signsNS[1] == True:
            NS[:,0] = -1*EVlow[:,NSIdx_low];
        else:
            NS[:,0] = EVlow[:,NSIdx_low];
            
        if signsNS[2] == True:
            NS[:,1] = -1*EVmed[:,NSIdx_med];
        else:
            NS[:,1] = EVmed[:,NSIdx_med];
            
        if signsNS[3] == True:
            NS[:,2] = -1*EVhigh[:,NSIdx_high];
        else:
            NS[:,2] = EVhigh[:,NSIdx_high];
        
        #differences
        NSdiff = np.zeros( (len(EVlow[:,0]), 3), dtype=complex );
        NSdiff[:,0] = NS[:,0] - NSstat
        NSdiff[:,1] = NS[:,1] - NSstat
        NSdiff[:,2] = NS[:,2] - NSstat
        
        #colorbar normalization
        vmaxNS = max(abs(NS[:,0:2].flatten()))
        vminNS = np.real( min(NS[:,0:2].flatten()) )
        normNS = matplotlib.colors.Normalize(vmax=vmaxNS, vmin=vminNS)
        
        vmaxNSdiff = max(abs(NSdiff[:,0:2].flatten()))
        vminNSdiff = np.real( min(NSdiff[:,0:2].flatten()) )
        normNSdiff = matplotlib.colors.Normalize(vmax=vmaxNSdiff, vmin=vminNSdiff)
        
        PlotEigenmodes(NS, NSdiff, triangulationOwn, normNS, normNSdiff, show=True, save=False, drawOrigin=True)
        
        #return to origin
        os.chdir(origin);
        
        