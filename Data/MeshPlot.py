from matplotlib import pyplot as plt
import numpy as np
import matplotlib.tri as tri

vertFile = open("Plot_Vertices.dat", 'r')
#vertFile = open("ID1_1_Coord.dat", 'r')

vertices = []
for line in vertFile:
    data = line.split();
    vertex = [float(data[0]), float(data[1]) ];
    vertices.append(vertex);
    
    
triFile = open("Plot_Elements.dat", 'r')
#triFile = open("ID1_1_Elements.dat", 'r')
triangles = []
for line in triFile:
    data = line.split();
    triangle = [int(data[0]), int(data[1]), int(data[2])]
    triangles.append(triangle)


EVFile = open("Eigenmodes_omega_3e+07.dat", 'r')
eigval = []
for line in EVFile:
    data = line.split();
    ev = [ float(x) for x in data]
    eigval.append(ev)

vertexArray = np.asarray(vertices)
triangleArray = np.asarray(triangles)
evArray = np.array(eigval)

triangulationOwn = tri.Triangulation(vertexArray[:,0], vertexArray[:,1], triangles=triangleArray)

for k in xrange(np.shape(evArray)[1]):
    plt.figure()
    plt.gca().set_aspect('equal')
    plt.tripcolor(triangulationOwn, evArray[:,k], shading='gouraud', edgecolors='k', cmap=plt.cm.viridis)
    plt.colorbar()
    plt.title("Eigenmode %d"%(k))
    plt.show()
    plt.savefig("Eigenmode_%d.png"%(k), format='png', papertype='a4', dpi=1000, orientation='landscape')
    plt.savefig("Eigenmode_%d.eps"%(k), format='eps', papertype='a4', dpi=1000, orientation='landscape')
    plt.close()