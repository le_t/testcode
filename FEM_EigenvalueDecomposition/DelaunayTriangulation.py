import numpy as np
from scipy.spatial import Delaunay

if __name__=='__main__':
	#radius
	R=2.0
	#number pie parts
	NAngular = 32
	#number of intervals to subdivide the radial direction into
	NRadial = 2
	#one offset for center point
	ptsCart = np.zeros((NAngular*NRadial+1,2))
	#angle subdivision
	phi=np.linspace(0,2.0*np.pi,NAngular)
	for i in range(NRadial):
		for j in xrange(NAngular):
			ptsCart[i*NAngular + j,0] = (R/NRadial * (i+1)) * np.cos(phi[j])
			ptsCart[i*NAngular + j,1] = (R/NRadial * (i+1)) * np.sin(phi[j])
	ptsCart[-1,0]=ptsCart[-1,1]=0.0
	bd = np.zeros([NAngular,2])
	for j in range(NAngular):
		bd[j,0] = j+NAngular*(NRadial-1)
		bd[j,1] = (j+1)%NAngular + NAngular*(NRadial-1)

	#compute delaunay triangulation
	tri = Delaunay(ptsCart)
        #files to store data
        CoordFile = open("Coordinates.dat", 'w')
        BdFile = open("Boundary.dat", 'w')
        MeshFile = open("Elements.dat", 'w')

        #write data
        for k in xrange(NAngular*NRadial+1):
            CoordFile.write("%f\t%f\n"%(ptsCart[k,0], ptsCart[k,1]))
        for j in xrange(NAngular):
            BdFile.write("%d\t%d\n"%(bd[j,0],bd[j,1]))
        CoordFile.close()
        BdFile.close()
        for s in xrange(len(tri.simplices)):
            MeshFile.write("%d\t%d\t%d\n"%(tri.simplices[s][0],tri.simplices[s][1],tri.simplices[s][2]))
        MeshFile.close()
