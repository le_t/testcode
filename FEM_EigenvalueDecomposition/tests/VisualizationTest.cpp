/* 
 * File:   VisualizationTest.cpp
 * Author: lebedev
 *
 * Created on 18.01.2016, 16:03:47
 */
#define IGL_VIEWER_WITH_NANOGUI
#include <stdlib.h>
#include <iostream>
#include "Geometry.h"
#include "Functions.h"
#include <igl/viewer/Viewer.h>

/*
 * Simple C++ Test Suite
 */
const uint triVert(6);


int main(int argc, char** argv) {
    
    std::string CoordFname("Koordinaten_ref.dat"), TriFname("Elemente_ref.dat"), BdFname("Boundary_ref.dat");

    params RefPar;
    //parameter input
    prog_opt_init(argc, argv, RefPar);

    //read-in
    geometry2 geo = readData2D(RefPar.CoordinateFilename, RefPar.ElementsFilename, RefPar.BoundaryFilenameD, RefPar.BoundaryFilenameN);
    
    ref2d(geo, RefPar.Nref);
    uint origNvert = geo.Nvert;
    p2basepoints(geo);
    
    //Eigen matrices for the data
    Eigen::MatrixXd V(geo.Nvert, 3);
    Eigen::MatrixXi F(geo.Nelem, 3);
    Eigen::MatrixXd V2(geo.Nvert-origNvert, 3);
    
    V.setZero();
    F.setZero();
    
    //copy vertices
    for(uint j(0); j<geo.Nvert; ++j){
        V(j,0) = geo.vertices[j].x[0];
        V(j,1) = geo.vertices[j].x[1];
    }
    //copy elements
    for(uint j(0); j<geo.Nelem; ++j){
        F(j,0) = geo.element[j].vert[0];
        F(j,1) = geo.element[j].vert[1];
        F(j,2) = geo.element[j].vert[2];
    }
    //copy midpoints
    for(uint j(origNvert); j<geo.Nvert; ++j){
        V2(j-origNvert,0) = geo.vertices[j].x[0];
        V2(j-origNvert,1) = geo.vertices[j].x[1];
    }
    //plot
    igl::viewer::Viewer window;
    window.data.set_mesh(V,F);
    window.data.add_points(V2, Eigen::RowVector3d(1,0.5,0));
    window.launch();

    return (EXIT_SUCCESS);
}


