/* 
 * File:   ArpackCshift.cpp
 * Author: lebedev
 *
 * Created on 20.07.2016, 14:32:38
 */
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include "Geometry.h"
#include "Functions.h"
#include "GaussQuadParam.h"

#include "../lib/Eigen_3.2.6/unsupported/Eigen/SparseExtra"
#include <Eigen/UmfPackSupport>
#include "arcomp.h"
#include "arrgsym.h"
#include "arrgnsym.h"

/*
 * Simple C++ Test Suite
 */

void test1() {
    std::cout << "ArpackCshift test 1" << std::endl;
}

void test2() {
    std::cout << "ArpackCshift test 2" << std::endl;
    std::cout << "%TEST_FAILED% time=0 testname=test2 (ArpackCshift) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% ArpackCshift" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    uint dim(100);
    //create matrices
    Eigen::SparseMatrix<dtype> A(dim, dim);
    Eigen::SparseMatrix<dtype> B(dim, dim);
    Eigen::VectorXi NnzPerRowA(dim), NnzPerRowB(dim);

    //fill the number of non-zeros - overestimate a bit
    for (uint row(0); row < dim; ++row) {
        NnzPerRowA(row) = 3;
        NnzPerRowB(row) = 3;
    }

    //reserve space
    A.reserve(NnzPerRowA);
    B.reserve(NnzPerRowB);

    //fill
    for (uint row(1); row < dim - 1; ++row) {
        //matrix A: diagonal: 2, subdiagonal: -2, superdiagonal: 3
        A.coeffRef(row, row) = 2;
        A.coeffRef(row, row - 1) = -2;
        A.coeffRef(row, row + 1) = 3;
        //matrix B: diagonal : 4 off-diagonal: 1
        B.coeffRef(row, row) = 4;
        B.coeffRef(row, row - 1) = B.coeffRef(row, row + 1) = 1;
    }
    //the first and last row
    A.coeffRef(0, 0) = 2;
    A.coeffRef(0, 1) = 3;
    A.coeffRef(dim - 1, dim - 1) = 2;
    A.coeffRef(dim - 1, dim - 2) = -2;
    B.coeffRef(0, 0) = 4;
    B.coeffRef(0, 1) = 1;
    B.coeffRef(dim - 1, dim - 1) = 4;
    B.coeffRef(dim - 1, dim - 2) = 1;


    //compress
    A.makeCompressed();
    B.makeCompressed();

    //misc vectors
    Vec w(dim), z(dim);
    cVec wC(dim), zC(dim);

    //solver
    cdtype ComplexShift(0.4, 0.6);
    Eigen::UmfPackLU < Eigen::SparseMatrix<cdtype> > Dsolver;
    Eigen::SparseMatrix<cdtype> D(dim, dim);
    D = B.cast<cdtype>();
    D *= -ComplexShift;
    D += A.cast<cdtype>();
    D.makeCompressed();

    Dsolver.compute(D);
    //set up arpack
    ARrcNonSymGenEig<dtype> prob(A.cols(), 4L, 'R', ComplexShift.real(), ComplexShift.imag());

    while (!prob.ArnoldiBasisFound()) {

        // Calling ARPACK FORTRAN code. Almost all work needed to
        // find an Arnoldi basis is performed by TakeStep.

        prob.TakeStep();

        switch (prob.GetIdo()) {
            case -1:

                // Performing w <- Re{OP*B*v} for the first time.
                // This product must be performed only if GetIdo is equal to
                // -1. GetVector supplies a pointer to the input vector, v,
                // and PutVector a pointer to the output vector, w.
                z = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(prob.GetVector(), z.rows());
                w = B*z;
                wC = w.cast<cdtype>();
                //solve
                zC = Dsolver.solve(wC);
                for (uint row(0); row < zC.rows(); ++row)
                    z(row) = zC(row).real();
                Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(prob.PutVector(), z.rows()) = z;
                break;

            case 1:

                // Performing w <- Real{OP*B*v} when Bv is available.
                // This product must be performed whenever GetIdo is equal to
                // 1. GetProd supplies a pointer to the previously calculated
                // product Bv and PutVector a pointer to the output vector w.

                z = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(prob.GetProd(), z.rows());
                wC = z.cast<cdtype>();
                //solve
                zC = Dsolver.solve(wC);
                for (uint row(0); row < zC.rows(); ++row)
                    w(row) = zC(row).real();
                Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(prob.PutVector(), w.rows()) = w;
                break;

            case 2:

                // Performing w <- B*v.
                z = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(prob.GetVector(), z.rows());
                w = B*z;
                Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(prob.PutVector(), w.rows()) = w;


        }
    }

    // Finding eigenvalues and eigenvectors.

    int nconv = prob.FindEigenvectors();

    // Recovering eigenvalues of the original problem
    // using the Rayleigh quotient.

    dtype num(0), denom(0), *evR, *evI, *rawEV;
    cdtype numC(0), denomC(0), quotient(0);
    uint ColJ(0), ColJp1(0);
    evR = prob.RawEigenvalues();
    evI = prob.RawEigenvaluesImag();
    rawEV = prob.RawEigenvectors();
    for (int i(0); i < nconv; ++i) {
        ColJ = i * A.cols();
        ColJp1 = ColJ + A.cols();
        if (evI[i] == 0.0) {
            //real eigenvalue
            z = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(&rawEV[ColJ], z.rows());
            w = A * z;
            num = w.dot(z);
            w = B * z;
            denom = w.dot(z);
            //store
            evR[i] = num / denom;
            std::cout << "lambda[" << i + 1 << "]: " << std::setprecision(10) << evR[i] << std::endl;
        }
        else {
            //complex eigenvalue
            //numerator
            z = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(&rawEV[ColJ], z.rows());
            w = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(&rawEV[ColJp1], w.rows());
            for (uint s(0); s < w.rows(); ++s)
                zC(s) = cdtype(z(s), w(s));
            wC = A.cast<cdtype>() * zC;
            numC = wC.dot(zC);



            //denominator
            z = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(&rawEV[ColJ], z.rows());
            w = Eigen::Matrix<dtype, Eigen::Dynamic, 1>::Map(&rawEV[ColJp1], w.rows());
            for (uint s(0); s < w.rows(); ++s)
                zC(s) = cdtype(z(s), w(s));
            wC = B.cast<cdtype>() * zC;
            denomC = wC.dot(zC);

            quotient = numC / denomC;
            evR[i] = quotient.real();
            evI[i] = quotient.imag();

            evR[i + 1] = std::conj(quotient).real();
            evI[i + 1] = std::conj(quotient).imag();
            i++;

            std::cout << "lambda[" << i + 1 << "]: " << std::setprecision(10) << " (" << evR[i] << ", " << evI[i] << " )" << std::endl;
            std::cout << "lambda[" << i + 2 << "]: " << std::setprecision(10) << " (" << evR[i + 1] << ", " << evI[i + 1] << " )" << std::endl;

        }
    }

    std::cout << "Dimension of the system            : " << prob.GetN() << std::endl;
    std::cout << "Number of 'requested' eigenvalues  : " << prob.GetNev() << std::endl;
    std::cout << "Number of 'converged' eigenvalues  : " << prob.ConvergedEigenvalues() << std::endl;
    std::cout << "Number of Arnoldi vectors generated: " << prob.GetNcv() << std::endl;
    std::cout << "Number of iterations taken         : " << prob.GetIter() << std::endl;
    std::cout << std::endl;

    for (uint j(0); j < prob.ConvergedEigenvalues(); ++j) {
        std::cout << "lambda[" << j + 1 << "]: " << std::setprecision(10) << prob.Eigenvalue(j) << std::endl;
    }

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

