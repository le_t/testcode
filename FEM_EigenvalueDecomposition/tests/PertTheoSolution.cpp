/* 
 * File:   PertTheoSolution.cpp
 * Author: lebedev
 * 
 * Implementation of an approximation of the eigenvalues of a rotating
 * domain using perturbation theory.
 *
 * Created on 26.06.2016, 17:09:12
 */


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include "Geometry.h"
#include "Functions.h"

#include "../lib/Eigen_3.2.6/unsupported/Eigen/ArpackSupport"
#include "../lib/Eigen_3.2.6/unsupported/Eigen/SparseExtra"
#include <Eigen/OrderingMethods>
#include "arcomp.h"
#include "arlnsmat.h"
#include "arlgnsym.h"
#include "arrsnsym.h"

using namespace std;



//------------------------------------------------------------------------------

int main(int argc, char** argv) {
    std::string CoordFname("Koordinaten_ref.dat"), TriFname("Elemente_ref.dat"), BdFname("Boundary_ref.dat");

    params RefPar;
    //parameter input
    prog_opt_init(argc, argv, RefPar);

    //read-in
    geometry2 geo = readData2D(RefPar.CoordinateFilename, RefPar.ElementsFilename, RefPar.BoundaryFilenameD, RefPar.BoundaryFilenameN);

    //pull the flag for recomputation of the system matrices
    bool recompute(RefPar.recompute);

    //refine
    ref2d(geo, RefPar.Nref);
    //add midpoints for P2 FE.
    p3basepoints(geo);

    //update function indices - compute number of free nodes
    uint freeNodes = geo.updateFIdx();
    uint QuadOrder(9);
  
    const dtype GD[][3] = {
        {0.124949503233232, 0.437525248383384, 0.205950504760887 / 2},
        {0.437525248383384, 0.124949503233232, 0.205950504760887 / 2},
        {0.437525248383384, 0.437525248383384, 0.205950504760887 / 2},
        {0.797112651860071, 0.165409927389841, 0.063691414286223 / 2},
        {0.797112651860071, 0.037477420750088, 0.063691414286223 / 2},
        {0.165409927389841, 0.797112651860071, 0.063691414286223 / 2},
        {0.165409927389841, 0.037477420750088, 0.063691414286223 / 2},
        {0.037477420750088, 0.797112651860071, 0.063691414286223 / 2},
        {0.037477420750088, 0.165409927389841, 0.063691414286223 / 2}
    };


    //create RHS
    Vec F(freeNodes);
    F.setZero();

    //vector to store # of non-zeros per row of the stiffness matrix A
    Eigen::VectorXi NnzPerRow(freeNodes);
    DetermineSparsityPattern(geo, NnzPerRow);

    dtype integral(0.0);

    //variables to hold the numerical value of the vertex and transformation measure
    dtype jac(0.0);
    //index of the function associated with a vertex (accounting for the skips and 0-based indexing)
    uint FuncIdx1(0);

    //supplementary variables
    Eigen::Matrix2d J;
    Eigen::Vector2d tmp;
    Eigen::Vector2d q;
    //supplementary variables
    Eigen::Vector2d dPhi1, dPhi2;
    Eigen::Matrix2d Jinv;
    uint FuncIdx2(0);

    //--------------------------------------------------------------------------
    //matrix storage filename handling
    std::string Afile, Mfile, Cfile, tmpString;
    std::stringstream converter;

    //convert the # of refinement steps into a string
    converter << RefPar.Nref;
    converter >> tmpString;

    Afile = RefPar.MatPrefix + "_" + tmpString + "_stiffness.mm";
    Mfile = RefPar.MatPrefix + "_" + tmpString + "_mass.mm";
    Cfile = RefPar.MatPrefix + "_" + tmpString + "_damping.mm";

    std::ifstream InputFile;

    //------------------------------------------------------------------------------
    //---------------- Matrix creation ---------------------------------------------
    std::cout << "Creating matrix A" << std::endl;
    //create stiffness matrix
    CSRMat A(freeNodes, freeNodes);
    A.reserve(NnzPerRow);
    //iterator variable
    uint k(0);

    InputFile.open(Afile.c_str());
    if (!recompute && InputFile.good()) {
        //file exists and can be opened, now close it again
        InputFile.close();
        //read matrix
        Eigen::loadMarket<CSRMat>(A, Afile);
    }
    else {
        std::cout << "Filling matrix A" << std::endl;
        //parallelize using OpenMP
        //private variables: J, Jinv, jac, integral, FuncIdx1, FuncIdx2, tmp
        //shared variables: A - matrix

#pragma omp parallel private(J, Jinv, jac, integral, FuncIdx1, FuncIdx2, dPhi1, dPhi2,tmp),shared(A)
        {
            //create a local copy of the matrix A to be filled
            CSRMat Alocal(freeNodes, freeNodes);
            Alocal.reserve(NnzPerRow);

            //temporary entry for filling
            dtype entry(0);

#pragma omp for nowait,schedule(static)
            for (k = 0; k < geo.Nelem; ++k) { //iteration over the elements(subintervals)
                Jacobian(geo.element[k], geo.vertices, J);
                //abosulte value of the determinant
                jac = fabs(J(0, 0) * J(1, 1) - J(1, 0) * J(0, 1));
                //compute the inverse transposed jacobian matrix
                InvJacobian(geo.element[k], geo.vertices, Jinv);

                for (uint i(0); i < triVert; ++i) { //iteration over the boundaries
                    for (uint j = i; j < triVert; ++j) {
                        //fetch the values of the boundaries of the subinterval
                        if (geo.isInDirichletBoundary[geo.element[k].vert[i]] == true || geo.isInDirichletBoundary[geo.element[k].vert[j]] == true)
                            continue; //skip the boundaries of the domain - functions equal to nil

                        //determine function index
                        FuncIdx1 = geo.fIdx[ geo.element[k].vert[i] ];
                        FuncIdx2 = geo.fIdx[ geo.element[k].vert[j] ];
                        //integrate
                        integral = 0.0;


                        for (uint s(0); s < QuadOrder; ++s) {
                            //g(geo.element[k], geo.vertices, q);
                            dPhi_3(i, tmp, GD[s][0], GD[s][1]);
                            dPhi1 = Jinv*tmp;
                            dPhi_3(j, tmp, GD[s][0], GD[s][1]);
                            dPhi2 = Jinv*tmp;
                            integral += GD[s][2] * dPhi1.dot(dPhi2);
                        }
                        integral *= jac; //transform the measure (constant)

                        //temporary entry - fill the real and imaginary parts of the matrix
                        // with the same values
                        entry = integral;


                        Alocal.coeffRef(FuncIdx1, FuncIdx2) += entry;
                        if (FuncIdx1 != FuncIdx2) {
                            //subdiagonal
                            Alocal.coeffRef(FuncIdx2, FuncIdx1) += entry;
                        }
                    }
                }
            }
            //finalize and update global stiffness matrix
            Alocal.makeCompressed();
#pragma omp critical (Aupdate)
            {
                //std::cout << "thread " << omp_get_thread_num() << " updating matrix" << std::endl;
                A += Alocal;
            }
#pragma omp barrier
            //end of omp parallel
        }
        //store matrix
        Eigen::saveMarket<CSRMat>(A, Afile);
    }
 
    //create the mass matrix
    std::cout << "Creating matrix M" << std::endl;
    CSRMat M(freeNodes, freeNodes);
    M.reserve(NnzPerRow);


    InputFile.open(Mfile.c_str());
    if (!recompute && InputFile.good()) {
        //file exists and can be opened, now close it again
        InputFile.close();
        //read matrix
        Eigen::loadMarket<CSRMat>(M, Mfile);
    }
    else {
        std::cout << "Filling matrix M" << std::endl;
        //parallelize using OpenMP
        //private variables: J, Jinv, jac, integral, FuncIdx1, FuncIdx2.
        //shared variables: A - matrix
        //iterator variable - defined before filling the stiffness matrix A

        //HERE was a BUG!!!
#pragma omp parallel private(J, jac, integral, FuncIdx1, FuncIdx2, tmp),shared(M)
        {
            CSRMat Mlocal(freeNodes, freeNodes);
            Mlocal.reserve(NnzPerRow);

            //temporary entry for filling
            dtype entry(0);

#pragma omp for nowait,schedule(static)
            for (k = 0; k < geo.Nelem; ++k) { //iteration over the elements(subintervals)
                Jacobian(geo.element[k], geo.vertices, J);
                //abosulte value of the determinant
                jac = fabs(J(0, 0) * J(1, 1) - J(1, 0) * J(0, 1));

                for (uint i(0); i < triVert; ++i) { //iteration over the boundaries
                    for (uint j = i; j < triVert; ++j) {
                        //fetch the values of the boundaries of the subinterval
                        if (geo.isInDirichletBoundary[geo.element[k].vert[i]] == true || geo.isInDirichletBoundary[geo.element[k].vert[j]] == true)
                            continue; //skip the boundaries of the domain - functions equal to nil

                        //determine function index
                        FuncIdx1 = geo.fIdx[ geo.element[k].vert[i] ];
                        FuncIdx2 = geo.fIdx[ geo.element[k].vert[j] ];
                        //integrate
                        integral = 0.0;

                        for (uint s(0); s < QuadOrder; ++s)
                            integral += GD[s][2] * Phi_3(i, GD[s][0], GD[s][1]) * Phi_3(j, GD[s][0], GD[s][1]);

                        integral *= jac; //transform the measure (constant)

                        //temporary entry - fill the real and imaginary parts of the matrix
                        // with the same values
                        entry = integral;

                        //diagonal and superdiagonal
                        Mlocal.coeffRef(FuncIdx1, FuncIdx2) += entry;
                        if (FuncIdx1 != FuncIdx2) {
                            //subdiagonal
                            Mlocal.coeffRef(FuncIdx2, FuncIdx1) += entry;
                        }
                    }
                }
            }
            //finalize and update
            Mlocal.makeCompressed();
#pragma omp critical (Mupdate)
            {
                //std::cout << "thread " << omp_get_thread_num() << " updating matrix" << std::endl;
                M += Mlocal;
            }
#pragma omp barrier
            //end of omp parallel
        }
        //store matrix
        Eigen::saveMarket<CSRMat>(M, Mfile);
    }

    //prune vanishing entries
    A.prune(RefPar.tol, 1e-12);
    M.prune(RefPar.tol, 1e-12);
    //compress the global sparse matrices
    A.finalize();
    M.finalize();
    A.makeCompressed();
    M.makeCompressed();

    //Use ARPACK to determine the eigenvalues
    dtype tol(RefPar.tol); //tolerance
    dtype sigma(0); //shift for later use
    uint Neig = RefPar.Neig;
    // Eigen::ArpackGeneralizedSelfAdjointEigenSolver does not work with complex matrices
    Eigen::ArpackGeneralizedSelfAdjointEigenSolver<CSRMat, Eigen::SimplicialLDLT<CSRMat>, true> arsolver;
    arsolver.compute(A, M, Neig, "SM");
    std::cout << "ARPACK status " << arsolver.info() << std::endl;
    std::cout << "Eigenvalues computed with ARPACK: " << std::setprecision(10) << arsolver.eigenvalues() << std::endl;


    //create the transport matrix
    std::cout << "Creating matrix C" << std::endl;
    CSRMat C(freeNodes, freeNodes);
    C.reserve(NnzPerRow);

    InputFile.open(Cfile.c_str());
    if (!recompute && InputFile.good()) {
        //file exists and can be opened, now close it again
        InputFile.close();
        //read matrix
        Eigen::loadMarket<CSRMat>(C, Cfile);
    }
    else {
        std::cout << "Filling matrix C" << std::endl;
        //parallelize using OpenMP
        //private variables: J, Jinv, jac, integral, FuncIdx1, FuncIdx2.
        //shared variables: C - matrix

#pragma omp parallel private(J, Jinv, jac, integral, dPhi1, dPhi2, FuncIdx1, FuncIdx2, tmp),shared(C)
        {
            CSRMat Clocal(freeNodes, freeNodes);
            Clocal.reserve(NnzPerRow);
            //temporary entry for filling
            dtype entry(0);

#pragma omp for nowait,schedule(static)
            for (k = 0; k < geo.Nelem; ++k) { //iteration over the elements(subintervals)
                Jacobian(geo.element[k], geo.vertices, J);
                //abosulte value of the determinant
                jac = fabs(J(0, 0) * J(1, 1) - J(1, 0) * J(0, 1));
                //compute the inverse transposed jacobian matrix
                InvJacobian(geo.element[k], geo.vertices, Jinv);

                for (uint i(0); i < triVert; ++i) { //iteration over the boundaries
                    for (uint j = i; j < triVert; ++j) {
                        //fetch the values of the boundaries of the subinterval
                        if (geo.isInDirichletBoundary[geo.element[k].vert[i]] == true || geo.isInDirichletBoundary[geo.element[k].vert[j]] == true)
                            continue; //skip the boundaries of the domain - functions equal to nil

                        //determine function index
                        FuncIdx1 = geo.fIdx[ geo.element[k].vert[i] ];
                        FuncIdx2 = geo.fIdx[ geo.element[k].vert[j] ];
                        //integrate
                        integral = 0.0;

                        for (uint s(0); s < QuadOrder; ++s) {
                            //phi_i*dphi_j
                            dPhi_3(j, tmp, GD[s][0], GD[s][1]);
                            dPhi1 = Jinv*tmp;
                            //v = (y,-x) -> -xd_y+yd_x
                            tmp(0) = GD[s][0];
                            tmp(1) = GD[s][1];
                            //Map the values from reference triangle back to the domain
                            dPhi2 = Jinv*tmp;
                            tmp(0) = dPhi2(1);
                            tmp(1) = -dPhi2(0);
                            //phi_j*dphi_i will be the same up to a sign!
                            integral += GD[s][2] * Phi_3(i, GD[s][0], GD[s][1]) * dPhi1.dot(tmp);
                        }

                        integral *= jac; //transform the measure (constant)

                        //temporary entry - fill the real and imaginary parts of the matrix
                        // with the same values
                        entry = integral;

                        //no diagonal terms

                        if (FuncIdx1 != FuncIdx2) {
                            //superdiagonal
                            Clocal.coeffRef(FuncIdx1, FuncIdx2) += entry;
                            //subdiagonal phi_j*dphi_i = -phi_i*dphi_j
                            Clocal.coeffRef(FuncIdx2, FuncIdx1) -= entry;
                        }
                    }
                }
            }
            //finalize and update
            Clocal.makeCompressed();
#pragma omp critical (Cupdate)
            {
                //std::cout << "thread " << omp_get_thread_num() << " updating matrix" << std::endl;
                C += Clocal;
            }
#pragma omp barrier
            //end of omp parallel
        }
        Eigen::saveMarket<CSRMat>(C, Cfile);
    }
    //prune vanishing entries
    C.prune(RefPar.tol, 1e-12);
    //compress
    C.finalize();
    C.makeCompressed();

    return 0;
}
