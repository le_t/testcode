/* 
 * File:   arpackpptest.cpp
 * Author: aquinox
 *
 * 
 * A simple test of ARPACK(++) from the new repositories.
 * The code is copied from the examples accompanying ARPACK++
 * and the compilation process is the modified to accomodate the library.
 * 
 * Created on 06.06.2016, 00:18:05
 */

#include <stdlib.h>
#include <iostream>


#include "arcomp.h"
#include "lcmatrxe.h"
#include "lcmatrxf.h"
#include "arunsmat.h"
#include "arugcomp.h"
#include "lcompsol.h"



int main()
{

  // Defining variables;

  int    n;              // Dimension of the problem.
  int    nconv;          // Number of "converged" eigenvalues.
  int    nnzA,   nnzB;   // Number of nonzero elements in A and B.
  int    *irowA, *irowB; // pointer to an array that stores the row
                         // indices of the nonzeros in A and B.
  int    *pcolA, *pcolB; // pointer to an array of pointers to the
                         // beginning of each column of A (B) in valA (valB).
  arcomplex<double> *valA,  *valB;  // pointer to an array that stores the nonzero
                         // elements of A and B.
  arcomplex<double> EigVal[101];    // Eigenvalues.
  arcomplex<double> EigVec[1001];   // Eigenvectors stored sequentially.
  char   uplo;           // Variable that indicates whether the upper
                         // (uplo='U') ot the lower (uplo='L') part of
                         // A and B will be supplied to AREig.

  // Creating matrices A and B.

  n = 10;
  uplo = 'U';
  //SymmetricMatrixC(n, nnzA, valA, irowA, pcolA);
  //SymmetricMatrixD(n, nnzB, valB, irowB, pcolB);

  /*
   for(uint k(0); k<n+1; ++k){
      std::cout<<"row["<<k<<"]="<<pcolA[k]<<std::endl;
    }
  std::cout<<nnzA<<std::endl;
  for(uint k(0); k<nnzA; ++k){
      std::cout<<"col["<<k<<"]="<<irowA[k]<<"\t";
      std::cout<<"val["<<k<<"]="<<valA[k]<<std::endl;
    }
  std::cout<<"\n";
  for(uint k(0); k<n; ++k){
      std::cout<<"pcolA["<<k<<"]="<<pcolA[k]<<"\t";
      std::cout<<"pcolA["<<k+1<<"]="<<pcolA[k+1]<<std::endl;
      for(uint s=pcolA[k]; s<pcolA[k+1];++s){
	  std::cout<<"row["<<s<<"]="<<irowA[s]<<"\t";
	  std::cout<<"val["<<s<<"]="<<valA[s]<<std::endl;
	}
    }
*/
  // Finding the four eigenvalues of A with largest magnitude and the
  // related eigenvectors.
  arcomplex<double> rho = arcomplex<double>(10.0, 0.0);
  CompMatrixE(n, rho, nnzA, valA, irowA, pcolA);
  ARumNonSymMatrix<arcomplex<double>, double> A(n, nnzA, valA, irowA, pcolA);

  CompMatrixF(n, nnzB, valB, irowB, pcolB);
  ARumNonSymMatrix<arcomplex<double>, double> B(n, nnzB, valB, irowB, pcolB);

  // Defining what we need: the four eigenvectors nearest to sigma.

  ARluCompGenEig<double> dprob(4L, A, B, arcomplex<double>(1.0,0.0));

  dprob.FindEigenvectors();

  // Printing solution.

  Solution(A, B, dprob);

} // main.