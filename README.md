# README #
# Overview

This repository contains the code, evaluation routines and results  prdoced during my investigation of isospectral domains subject to uniform rotation.
It contains my own implementation of cubic finite elements, red-green mesh refinement, OpenMP parallelized construction of system matrices and 
a solution of a (parametrized) gyroscopic quadratic eigenvalue problem.

This parametric solver has been used to investigate the behaviour of rotating 2D isospectral domains.

**The development has been done on the branch HO\_TriFEM, which has now been merged into the default branch**

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact